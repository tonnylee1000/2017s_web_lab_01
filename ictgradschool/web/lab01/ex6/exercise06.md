Morrning Tonny!
Again.
Markdown is a lightweight markup language that uses a very basic syntax to describe documents. 
The Markdown syntax is appealing to users as it can easily be read almost as easily in its 
plain-text form as it can be when it is rendered. This makes it an ideal format to use for 
lightweight documentation tasks that might be consumed through a web browser or through a text
editor. 

# hello, This is Markdown Live Preview

----
## what is Markdown?
see [Wikipedia](http://en.wikipedia.org/wiki/Markdown)

> Markdown is a lightweight markup language, originally created by John Gruber and Aaron Swartz allowing people "to write using an easy-to-read, easy-to-write plain text format, then convert it to structurally valid XHTML (or HTML)".

----
## usage
1. Write markdown text in this textarea.
2. Click 'HTML Preview' button.

----
## markdown quick reference
# headers

*emphasis*

**strong**

* list

>block quote

    code (4 spaces indent)
[links](http://wikipedia.org)

----
## changelog
* 17-Feb-2013 re-design

----
## thanks
* [markdown-js](https://github.com/evilstreak/markdown-js)
